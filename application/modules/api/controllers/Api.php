<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('Api_model');
	}


	public function get_chart_2g_trx() {

		$result = $this->Api_model->get_chart_2g_trx();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}


	public function get_chart_2g_power() {

		$result = $this->Api_model->get_chart_2g_power();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_chart_3g_cell() {

		$result = $this->Api_model->get_chart_3g_cell();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_chart_3g_power() {

		$result = $this->Api_model->get_chart_3g_power();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}


	public function get_chart_4g_cell() {

		$result = $this->Api_model->get_chart_4g_cell();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_chart_4g_power() {

		$result = $this->Api_model->get_chart_4g_power();
		$rdata = array();
		if ($result){
			$i = 0;
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
				$i++;
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_chart_4g_bw() {

		$result = $this->Api_model->get_chart_4g_bw();
		$rdata = array();
		if ($result){
			$i = 0;
			foreach ($result as $row) {
				$rdata[] =  array(
					'WEEK' => $row->WEEK,
					'ACTIVATED' => (int)$row->ACTIVATED,
					'DEACTIVATED' => (int)$row->DEACTIVATED,
					'TOTAL' => (int)$row->TOTAL
				);
				$i++;
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function index()
	{
		echo "success";
	}

}
